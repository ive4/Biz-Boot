package com.flycms.common.entity;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 树形数据实体接口
 * @param <E>
 * @author 孙开飞
 * @date 2018年3月22日
 */
public interface TreeEntity<E> {
    public Long getId();
    public Long getParentId();
    public void setChildren(List<E> children);
}
