package com.flycms.modules.links.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.common.validator.Sort;
import com.flycms.modules.links.entity.Links;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @Description: 企业友情链接管理页面
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:06 2019/8/17
 */
@Controller
@RequestMapping("/member/links/")
public class LinksMemberController extends BaseController {

    @GetMapping("/list{url.suffix}")
    public String siteList(@RequestParam(value = "siteId", defaultValue = "0") String siteId, Model model)
    {
        model.addAttribute("siteId",siteId);
        return "system/member/site/links/list";
    }

    @GetMapping("/listData{url.suffix}")
    @ResponseBody
    public Object siteList(Links links,
                           @RequestParam(value = "page",defaultValue = "1") Integer page,
                           @RequestParam(value = "pageSize",defaultValue = "10") Integer pageSize,
                           @Sort(accepts = {"create_time", "id"}) @RequestParam(defaultValue = "create_time") String sort,
                           @RequestParam(defaultValue = "desc") String order) {

        return linksService.selectLinksLayListPager(links, page, pageSize, sort, order);
    }
}
