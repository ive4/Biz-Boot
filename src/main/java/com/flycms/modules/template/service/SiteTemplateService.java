package com.flycms.modules.template.service;

import com.flycms.common.pager.Pager;
import com.flycms.modules.template.entity.SiteTemplateOrder;
import com.flycms.modules.template.entity.SiteTemplatePage;
import freemarker.template.Template;
import org.springframework.ui.Model;

import java.util.HashMap;
import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 15:59 2019/9/10
 */
public interface SiteTemplateService {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 获取企业网站模板
     *
     * @param siteId
     *         网站id
     * @param templateName
     *         模板文件名
     * @return
     */
    public String getSiteTemplate(Long siteId,String templateName);

    //按id查询模板信息
    public SiteTemplatePage findById(Long id);

    /**
     * 按网站id、模板id、和模型id，查询模板页面列表
     *
     * @param siteId
     * @param templateId
     * @param moduleId
     * @return
     */
    public List<SiteTemplatePage> selectTemplatePageList(Long siteId, Long templateId, Long moduleId, Integer pageType);

    //数据库模板读取通过freemarker解析输出整个完整页面
    public Object exportTemplate(String templatePage,Model map);

    /**
     * 查询用户购买模板列表翻页
     *
     * @param siteId
     * @param page
     * @param pageSize
     * @param sort
     * @param order
     * @return
     */
    public Pager<SiteTemplateOrder> selectTemplatePager(Long siteId, Integer page, Integer pageSize, String sort, String order);
}
