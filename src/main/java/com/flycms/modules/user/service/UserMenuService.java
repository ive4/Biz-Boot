package com.flycms.modules.user.service;

import com.flycms.common.entity.Ztree;
import com.flycms.modules.user.entity.UserMenu;
import com.flycms.modules.user.entity.UserMenuVO;
import com.flycms.modules.user.entity.UserRole;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 菜单 业务层
 * 
 * @author 孙开飞
 */
public interface UserMenuService {

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    /**
     * 新增保存菜单信息
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public Object insertMenu(UserMenu menu);

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 删除菜单管理信息
     *
     * @param id 菜单ID
     * @return 结果
     */
    public Object deleteMenuById(Long id);
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    /**
     * 修改保存菜单信息
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public Object updateMenu(UserMenu menu);

    /**
     * 修改菜单是否显示
     *
     * @param visible
     *         菜单状态（1显示 0隐藏）
     * @param id
     *         需要更新的信息id
     * @return
     *         返回成功条数
     */
    public Object updateMenuVisible(Boolean visible, Long id);
    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 校验菜单名称是否唯一
     *
     * @param menuName 菜单名称
     * @param parentId 父菜单ID
     * @param id 需要排除的id
     * @return 结果
     */
    public boolean checkMenuNameUnique(String menuName, Long parentId, Long id);

    /**
     * 根据菜单ID查询信息
     *
     * @param id 菜单ID
     * @return 菜单信息
     */
    public UserMenu findById(Long id);



    /**
     * 根据用户ID查询菜单
     * 
     * @param userId 用户信息
     * @return 菜单列表
     */
    public List<UserMenu> selectMenusByUser(Long userId);

    /**
     * 查询系统菜单列表
     * 
     * @param menu 菜单信息
     * @param userId 用户ID
     * @return 菜单列表
     */
    public List<UserMenu> selectMenuList(UserMenu menu, Long userId);

    /**
     * 查询菜单集合
     * 
     * @param userId 用户ID
     * @return 所有菜单信息
     */
    public List<UserMenu> selectMenuAll(Long userId);

    /**
     * 根据用户ID查询权限
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
    public Set<String> selectPermsByUserId(Long userId);

    /**
     * 根据角色ID查询菜单
     * 
     * @param role 角色对象
     * @param userId 用户ID
     * @return 菜单列表
     */
    public List<Ztree> roleMenuTreeData(UserRole role, Long userId);

    /**
     * 查询所有菜单信息
     * 
     * @param userId 用户ID
     * @return 菜单列表
     */
    public List<Ztree> menuTreeData(Long userId);

    /**
     * 查询系统所有权限
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
    public Map<String, String> selectPermsAll(Long userId);

    public List<UserMenu> selectMenuByParentId(Long parentId);

    /**
     * 按父ID查询菜单列表
     *
     * @param parentId 菜单父ID
     * @return 结果
     */
    public int selectCountMenuByParentId(Long parentId);

    public List<UserMenuVO> selectMenusTree();

    /**
     * 查询菜单使用数量
     * 
     * @param menuId 菜单ID
     * @return 结果
     */
    public int selectCountRoleMenuByMenuId(Long menuId);



}
