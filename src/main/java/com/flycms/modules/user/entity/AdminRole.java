package com.flycms.modules.user.entity;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 角色表 fly_role
 * 
 * @author 孙开飞
 */
@Setter
@Getter
public class AdminRole implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 角色ID */
    private Long id;

    /** 角色名称 */
    private String roleName;

    /** 角色权限 */
    private String roleKey;

    /** 角色状态（1正常0停用） */
    private Boolean status;

    /**  创建者 */
    private String createBy;

    /**  创建时间 */
    private LocalDateTime createTime;

    /**  更新者 */
    private String updateBy;

    /**  更新时间 */
    private LocalDateTime updateTime;

    /**  备注 */
    private String remark;

    /** 角色排序 */
    private String sortOrder;

    /** 删除标志（0代表存在 2代表删除） */
    private String deleted;
    /** 菜单组 */
    private Long[] menuIds;
}
