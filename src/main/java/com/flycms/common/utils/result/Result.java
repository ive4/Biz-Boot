package com.flycms.common.utils.result;

import java.io.Serializable;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * json数据返回实体类
 *
 * @author sunkaifei
 */

public class Result implements Serializable {
    private static final long serialVersionUID = 1L;
    public static int CODE_SUCCESS = 0;
    public static int CODE_FAILURED = -1;
    public static String[] NOOP = new String[0];
    private int code;
    private String message;
    private Object data;
    private String goUrl;

    private Result(int code, String message) {
        this.code = code;
        this.message = message;
    }

    private Result(int code, String message, String goUrl) {
        this.code = code;
        this.message = message;
        this.goUrl = goUrl;
    }

    private Result(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
    private Result(int code, String message, String goUrl, Object data) {
        this.code = code;
        this.message = message;
        this.goUrl = goUrl;
        this.data = data;
    }

    public static final Result success() {
        return new Result(CODE_SUCCESS, "操作成功");
    }

    public static final Result success(String message) {
        return new Result(CODE_SUCCESS, message);
    }

    public static final Result success(Object data) {
        return new Result(CODE_SUCCESS, "操作成功", data);
    }

    public static final Result success(String message, String goUrl) {
        return new Result(CODE_SUCCESS, message,goUrl);
    }

    public static final Result success(String message, Object data) {
        return new Result(CODE_SUCCESS, message, data);
    }

    public static final Result success(String message, String url, Object data) {
        return new Result(CODE_SUCCESS, message,url, data);
    }


    public static final Result failure() {
        return failure(CODE_FAILURED, "操作失败");
    }

    public static final Result failure(String message) {
        return failure(CODE_FAILURED, message);
    }

    public static final Result failure(int code, String message) {
        return new Result(code, message);
    }


    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }
    public String getGoUrl() {
        return this.goUrl;
    }

    public void setGoUrl(String goUrl) {
        this.goUrl = goUrl;
    }

    @Override
    public String toString() {
        return "{code:\"" + this.code + "\", message:\"" + this.message + "\", url:\"" + this.goUrl + "\", data:\"" + this.data.toString() + "\"}";
    }


}