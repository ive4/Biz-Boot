package com.flycms.modules.template.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.template.entity.SiteTemplateTags;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface SiteTemplateTagsDao extends BaseDao<SiteTemplateTags> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 查询网站id和类别查询模板名称是否重复
     *
     * @param siteId
     *         网站id
     * @param templateId
     *         模板id
     * @param tagKey
     *         用户自定义模板名称
     * @param id
     *         需要排除id
     * @return
     */
    public int checkTemplateTags(@Param("siteId") Long siteId,
                                 @Param("templateId") Long templateId,
                                 @Param("tagKey") String tagKey,
                                 @Param("id") Long id
    );
}
