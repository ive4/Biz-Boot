package com.flycms.modules.company.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.modules.company.entity.Company;
import com.flycms.modules.company.service.CompanyService;
import com.flycms.modules.shiro.ShiroUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 帮助说明
 * @author 孙开飞
 */
@Controller
@RequestMapping("/member/company")
public class CompanyMemberController extends BaseController {

	@GetMapping("/info{url.suffix}")
	public String add(Model model)
	{
	    //查询当前登录用户是否已关联企业信息
        Company company=companyService.findCompanyByUser(ShiroUtils.getLoginUser().getId());
        if(company!=null){
            model.addAttribute("company",company);
        }
		return "system/member/company/info";
	}

	//保存企业基本信息
	@PostMapping("/info{url.suffix}")
	@ResponseBody
	public Object info(Company company)
	{
		return companyService.addAndModifyCompany(company);
	}
	
}
