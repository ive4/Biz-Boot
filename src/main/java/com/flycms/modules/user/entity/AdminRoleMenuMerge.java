package com.flycms.modules.user.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 角色和菜单关联 fly_role_menu
 * 
 * @author 孙开飞
 */
@Setter
@Getter
public class AdminRoleMenuMerge {

    private  Long id;
    /** 角色ID */
    private Long roleId;
    
    /** 菜单ID */
    private Long menuId;
}
