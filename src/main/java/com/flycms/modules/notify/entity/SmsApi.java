package com.flycms.modules.notify.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class SmsApi implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;
    //接口名称
    private String apiName;
    //接口密钥id
    private String accessKeyId;
    //接口密钥
    private String accessKeySecret;
    //接口官网
    private String apiUrl;
    //添加时间
    private String createTime;
    //状态
    private String status;
}