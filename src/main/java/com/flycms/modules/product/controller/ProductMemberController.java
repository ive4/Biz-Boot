package com.flycms.modules.product.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.common.exception.ApiAssert;
import com.flycms.common.utils.result.Result;
import com.flycms.modules.company.entity.Company;
import com.flycms.modules.product.entity.Product;
import com.flycms.modules.product.entity.ProductDO;
import com.flycms.modules.product.service.ProductService;
import com.flycms.modules.shiro.ShiroUtils;
import com.flycms.modules.site.entity.SiteColumn;
import net.sf.json.JSONArray;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 17:04 2019/8/14
 */
@Controller
@RequestMapping("/member/product/")
public class ProductMemberController extends BaseController {

    @RequestMapping("/add{url.suffix}")
    public String addProduct(@RequestParam(value = "siteId", required = false) String siteId,
                             @RequestParam(value = "columnId", required = false) String columnId,
                             Model model){
        ApiAssert.notTrue(siteId == null || !NumberUtils.isNumber(siteId), "网站id为空或者类型错误");
        ApiAssert.notTrue(!siteCompanyService.checkSiteCompany(Long.parseLong(siteId)), "您不是网站管理员，请勿非法操作");
        ApiAssert.notTrue(columnId == null || !NumberUtils.isNumber(columnId), "分类id为空或者类型错误");
        SiteColumn column=siteColumnService.findByIdAndSiteId(Long.parseLong(columnId),Long.parseLong(siteId));
        ApiAssert.notTrue(column == null, "该分类不存在");
        List<String> fatherNode = siteColumnService.resultNodeList(Long.parseLong(siteId),Long.parseLong(columnId));
        Collections.reverse(fatherNode);
        JSONArray json  =  JSONArray.fromObject(fatherNode);
        String  fatherNodeJson  =  json.toString();
        model.addAttribute("fatherNode",fatherNodeJson);
        model.addAttribute("siteId",siteId);
        model.addAttribute("column",column);
        return theme.getAdminTemplate("system/member/site/product/add");
    }

    @PostMapping("/add{url.suffix}")
    @ResponseBody
    public Object addProduct(ProductDO productDO)
    {
        ApiAssert.notTrue(productDO.getSiteId() == null || !NumberUtils.isNumber(productDO.getSiteId()), "网站id为空或者类型错误");
        ApiAssert.notTrue(!siteCompanyService.checkSiteCompany(Long.parseLong(productDO.getSiteId())), "您不是网站管理员，请勿非法操作");
        ApiAssert.notTrue(productDO.getColumnId() == null || !NumberUtils.isNumber(productDO.getColumnId()), "分类id为空或者类型错误");
        SiteColumn column=siteColumnService.findByIdAndSiteId(Long.parseLong(productDO.getColumnId()),Long.parseLong(productDO.getSiteId()));
        ApiAssert.notTrue(column == null, "该分类不存在");
        if(StringUtils.isEmpty(productDO.getTitle())){
            return Result.failure("标题不能为空");
        }
        if(StringUtils.isEmpty(productDO.getContent())){
            return Result.failure("内容不能为空");
        }
        Company company=companyService.findCompanyByUser(ShiroUtils.getLoginUser().getId());
        Product product=new Product();
        product.setTitle(productDO.getTitle());
        product.setTitlepic(productDO.getTitlepic());
        product.setColumnId(Long.parseLong(productDO.getColumnId()));
        product.setSiteId(Long.parseLong(productDO.getSiteId()));
        product.setKeywords(productDO.getKeywords());
        product.setDescription(productDO.getDescription());
        product.setContent(productDO.getContent());
        product.setStatus(productDO.getStatus());
        return productService.addUserProduct(product,company.getId(),2);
    }
}
